// A simple program that computes the square root of a number
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <msgpack.hpp>
#include <tuple>

#include "../src/someFile.h"
#include "../src/SimpleRpcClient.h"

using namespace std;
using namespace AmqpClient;

int main()
{
  cout << "hello World. Docker" << endl;

  string hostname("myrabbitmqhostname");
  string rpc_queue("rpc_queue");
  int port(5672);
  string username("guest");
  string password("guest");

  //establish connection
  int frame_max(131072 / 32); // 131072
  string vhost("/");
  Channel::ptr_t channel = Channel::Create(hostname, port, username, password, vhost, frame_max);
  //auto opts = Channel::OpenOpts::()
  //Channel::ptr_t channel2 = Channel::Open(opts);
  //
  channel->DeclareQueue(rpc_queue, false, false, false, true);

  SimpleRpcClient::ptr_t simpleClient = SimpleRpcClient::Create(channel, rpc_queue);

  string requestMessage("Hallo?");

  //int requestMsg(1);
  while (1)
  {
    boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
    auto response = simpleClient->Call(requestMessage);

    msgpack::object_handle oh = msgpack::unpack(response.data(), response.size());
    // deserialized object is valid during the msgpack::object_handle instance is alive.
    msgpack::object deserialized = oh.get();

    // msgpack::object supports ostream.
    std::cout << deserialized << std::endl;

    // convert msgpack::object instance into the original type.
    // if the type is mismatched, it throws msgpack::type_error exception.
    msgpack::type::tuple<int, bool, std::string> dst;
    deserialized.convert(dst);

    //dst.
    cout << "message: " << dst.get<0>() << " " << dst.get<1>() << " " << dst.get<2>() << "\t";
    boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
    boost::posix_time::time_duration diff = t2 - t1;
    cout << "Time in millisec: \t" << 1.0 * diff.total_microseconds() / 1000 << endl;
    //cout << response << endl;
  }
  return 0;
}